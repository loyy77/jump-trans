package com.shixihub.ssh;

/**
 * @author LiShixi 2021/7/2 4:55 下午
 * <p>
 * This program will demonstrate SSH through jump hosts.
 * Suppose that you don't have direct accesses to host2 and host3.
 * $ CLASSPATH=.:../build javac JumpHosts.java
 * $ CLASSPATH=.:../build java JumpHosts usr1@host1 usr2@host2 usr3@host3
 * You will be asked passwords for those destinations,
 * and if everything works fine, you will get file lists of your home-directory
 * at host3.
 * <p>
 * This program will demonstrate SSH through jump hosts.
 * Suppose that you don't have direct accesses to host2 and host3.
 * $ CLASSPATH=.:../build javac JumpHosts.java
 * $ CLASSPATH=.:../build java JumpHosts usr1@host1 usr2@host2 usr3@host3
 * You will be asked passwords for those destinations,
 * and if everything works fine, you will get file lists of your home-directory
 * at host3.
 * <p>
 * This program will demonstrate SSH through jump hosts.
 * Suppose that you don't have direct accesses to host2 and host3.
 * $ CLASSPATH=.:../build javac JumpHosts.java
 * $ CLASSPATH=.:../build java JumpHosts usr1@host1 usr2@host2 usr3@host3
 * You will be asked passwords for those destinations,
 * and if everything works fine, you will get file lists of your home-directory
 * at host3.
 * <p>
 * This program will demonstrate SSH through jump hosts.
 * Suppose that you don't have direct accesses to host2 and host3.
 * $ CLASSPATH=.:../build javac JumpHosts.java
 * $ CLASSPATH=.:../build java JumpHosts usr1@host1 usr2@host2 usr3@host3
 * You will be asked passwords for those destinations,
 * and if everything works fine, you will get file lists of your home-directory
 * at host3.
 */
/* -*-mode:java; c-basic-offset:2; indent-tabs-mode:nil -*- */
/**
 * This program will demonstrate SSH through jump hosts.
 * Suppose that you don't have direct accesses to host2 and host3.
 *   $ CLASSPATH=.:../build javac JumpHosts.java
 *   $ CLASSPATH=.:../build java JumpHosts usr1@host1 usr2@host2 usr3@host3
 * You will be asked passwords for those destinations,
 * and if everything works fine, you will get file lists of your home-directory
 * at host3.
 *
 */

import com.jcraft.jsch.*;
import com.shixihub.view.HostEntity;

import javax.swing.*;
import java.awt.*;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;


public class JumpHostHelper {

    public static void conn(HostEntity targetHost, List<HostEntity> hosts) {
        if (hosts.isEmpty()) {
            System.err.println("未配置主机");
            return;
        }
        try {
            JSch jsch = new JSch();

            Session session = null;
            Session[] sessions = new Session[hosts.size()];

            String host = targetHost.getIpAddress();
            String user = targetHost.getUsername();
            String password = targetHost.getPassword();
            //host = host.substring(host.indexOf('@')+1);

            sessions[0] = session = jsch.getSession(user, host, 22);
            //session.setUserInfo(new MyUserInfo());
            // 不检查key，https://cloud.tencent.com/developer/ask/71527
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);

            session.setPassword(password);
            session.connect(60 * 1000);
            System.out.println("The session has been established to " + user + "@" + host);

            for (int i = 0; i < hosts.size(); i++) {
                final HostEntity curr = hosts.get(i);
                host = curr.getIpAddress();
                user = curr.getUsername();
                password = curr.getPassword();
                //host = host.substring(host.indexOf('@')+1);

                int assinged_port = session.setPortForwardingL(0, host, 22);
                System.out.println("portforwarding: " +
                        "localhost:" + assinged_port + " -> " + host + ":" + 22);
                sessions[i] = session =
                        jsch.getSession(user, "127.0.0.1", assinged_port);
                session.setConfig(config);
                //session.setUserInfo(new MyUserInfo());
                session.setPassword(password);
                session.setHostKeyAlias(host);
                session.connect(30 * 1000);
                System.out.println("The session has been established to " +
                        user + "@" + host);
            }
            final ChannelExec exec = (ChannelExec) session.openChannel("exec");
            exec.setCommand("date");
            final InputStream inputStream = exec.getInputStream();
            exec.setErrStream(System.out);

            byte[] buff = new byte[2048];
            StringBuffer ss = new StringBuffer();
            int len = 0;
            while ((len = inputStream.read(buff, 0, len)) != 0) {
                ss.append(Arrays.toString(buff));
            }
            inputStream.close();
            System.out.println("len = " + ss.toString());
            ChannelSftp sftp = (ChannelSftp) session.openChannel("sftp");

            sftp.connect();

            sftp.ls(".",
                    new ChannelSftp.LsEntrySelector() {
                        @Override
                        public int select(ChannelSftp.LsEntry le) {
                            System.out.println(le);
                            return ChannelSftp.LsEntrySelector.CONTINUE;
                        }
                    });
            sftp.disconnect();

            for (int i = sessions.length - 1; i >= 0; i--) {
                sessions[i].disconnect();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static class MyUserInfo implements UserInfo, UIKeyboardInteractive {
        @Override
        public String getPassword() {
            return passwd;
        }

        @Override
        public boolean promptYesNo(String str) {
            Object[] options = {"yes", "no"};
            int foo = JOptionPane.showOptionDialog(null,
                    str,
                    "Warning",
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    null, options, options[0]);
            return foo == 0;
        }

        String passwd;
        JTextField passwordField = (JTextField) new JPasswordField(20);

        @Override
        public String getPassphrase() {
            return null;
        }

        @Override
        public boolean promptPassphrase(String message) {
            return true;
        }

        @Override
        public boolean promptPassword(String message) {
            Object[] ob = {passwordField};
            int result =
                    JOptionPane.showConfirmDialog(null, ob, message,
                            JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                passwd = passwordField.getText();
                return true;
            } else {
                return false;
            }
        }

        @Override
        public void showMessage(String message) {
            JOptionPane.showMessageDialog(null, message);
        }

        final GridBagConstraints gbc =
                new GridBagConstraints(0, 0, 1, 1, 1, 1,
                        GridBagConstraints.NORTHWEST,
                        GridBagConstraints.NONE,
                        new Insets(0, 0, 0, 0), 0, 0);
        private Container panel;

        @Override
        public String[] promptKeyboardInteractive(String destination,
                                                  String name,
                                                  String instruction,
                                                  String[] prompt,
                                                  boolean[] echo) {
            panel = new JPanel();
            panel.setLayout(new GridBagLayout());

            gbc.weightx = 1.0;
            gbc.gridwidth = GridBagConstraints.REMAINDER;
            gbc.gridx = 0;
            panel.add(new JLabel(instruction), gbc);
            gbc.gridy++;

            gbc.gridwidth = GridBagConstraints.RELATIVE;

            JTextField[] texts = new JTextField[prompt.length];
            for (int i = 0; i < prompt.length; i++) {
                gbc.fill = GridBagConstraints.NONE;
                gbc.gridx = 0;
                gbc.weightx = 1;
                panel.add(new JLabel(prompt[i]), gbc);

                gbc.gridx = 1;
                gbc.fill = GridBagConstraints.HORIZONTAL;
                gbc.weighty = 1;
                if (echo[i]) {
                    texts[i] = new JTextField(20);
                } else {
                    texts[i] = new JPasswordField(20);
                }
                panel.add(texts[i], gbc);
                gbc.gridy++;
            }

            if (JOptionPane.showConfirmDialog(null, panel,
                    destination + ": " + name,
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE)
                    == JOptionPane.OK_OPTION) {
                String[] response = new String[prompt.length];
                for (int i = 0; i < prompt.length; i++) {
                    response[i] = texts[i].getText();
                }
                return response;
            } else {
                return null;  // cancel
            }
        }
    }
}
