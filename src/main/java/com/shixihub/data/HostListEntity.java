package com.shixihub.data;

import com.shixihub.view.HostEntityClone;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 目标主机列表
 * @author LiShixi 2021/7/2 8:16 下午
 */
public class HostListEntity implements Serializable {
    public String groupName;
    public String label;
    public HostEntityClone targetHost;
    public List<HostEntityClone> proxyHosts = new ArrayList<>();
}
