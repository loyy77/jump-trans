package com.shixihub.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.util.converter.DefaultStringConverter;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author LiShixi 2021/7/2 10:14 上午
 */
public class HostManagerController implements Initializable {
    public Button buttonAdd;
    public Button buttonDelete;
    public Button buttonTest;
    public TableView tableViewHost;
    public ObservableList<HostEntity> observableListHost = FXCollections.observableArrayList();

    public void onAdd(ActionEvent actionEvent) {
        final HostEntity hostEntity = new HostEntity();
        hostEntity.setIpAddress("127.0.0.1");
        hostEntity.setName("localhost");
        hostEntity.setUsername("apple");
        hostEntity.setPassword("sa");
        hostEntity.setOp(hostEntity);
        tableViewHost.getItems().add(hostEntity);

    }

    public void onDel(ActionEvent actionEvent) {
        tableViewHost.getItems().remove(tableViewHost.getSelectionModel().getSelectedItem());
    }

    public void onTestConn(ActionEvent actionEvent) {
        System.out.println("测试连接");
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //tableViewHost = new TableView<>(observableListHost);
        tableViewHost.setItems(observableListHost);
        tableViewHost.setEditable(true);
        tableViewHost.setPrefHeight(300);
        final TableColumn<HostEntity, Long> tableColumnId = new TableColumn<>("编号");
        final TableColumn<HostEntity, Boolean> tableColumnChecked = new TableColumn<>("选择");
        final TableColumn<HostEntity, String> tableColumnName = new TableColumn<>("名称");
        final TableColumn<HostEntity, String> tableColumnIpAddress = new TableColumn<>("IP");
        final TableColumn<HostEntity, String> tableColumnUserName = new TableColumn<>("用户名");
        final TableColumn<HostEntity, String> tableColumnPassword = new TableColumn<>("密码");
        final TableColumn<HostEntity, HostEntity> tableColumnOp = new TableColumn<>("操作");

        tableColumnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableColumnChecked.setCellValueFactory(new PropertyValueFactory<>("checked"));
        tableColumnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableColumnIpAddress.setCellValueFactory(new PropertyValueFactory<>("ipAddress"));
        tableColumnUserName.setCellValueFactory(new PropertyValueFactory<>("username"));
        tableColumnPassword.setCellValueFactory(new PropertyValueFactory<>("password"));
        tableColumnOp.setCellValueFactory(new PropertyValueFactory<>("op"));


        tableColumnChecked.setCellFactory(CheckBoxTableCell.forTableColumn(tableColumnChecked));
        tableColumnName.setCellFactory(TextFieldTableCell.forTableColumn(new DefaultStringConverter()));
        tableColumnIpAddress.setCellFactory(TextFieldTableCell.forTableColumn(new DefaultStringConverter()));
        tableColumnUserName.setCellFactory(TextFieldTableCell.forTableColumn(new DefaultStringConverter()));
        tableColumnPassword.setCellFactory(TextFieldTableCell.forTableColumn(new DefaultStringConverter()));
        tableColumnOp.setCellFactory(param -> {
            final TableCell<HostEntity, HostEntity> entityTableCell = new TableCell<HostEntity, HostEntity>() {
                @Override
                protected void updateItem(HostEntity item, boolean empty) {
                    super.updateItem(item, empty);
                    setGraphic(null);
                    setTextFill(null);
                    if (!empty) {
                        final Button button = new Button("测试");
                        button.setOnAction(event -> {
                            if (item.getIpAddress().isEmpty()) {
                                button.setTextFill(Paint.valueOf("#000000"));
                            } else {
                                button.setTextFill(Paint.valueOf("#33dd33"));
                            }
                        });
                        setGraphic(new Pane(button));
                    }
                }
            };
            return entityTableCell;
        });
        tableViewHost.getColumns().clear();
        tableViewHost.getColumns().addAll(
                tableColumnChecked,
                tableColumnName,
                tableColumnIpAddress,
                tableColumnUserName,
                tableColumnPassword,
                tableColumnOp
        );
    }
}
