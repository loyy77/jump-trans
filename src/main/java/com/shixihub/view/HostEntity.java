package com.shixihub.view;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;

/**
 * @author LiShixi 2021/7/2 11:04 上午
 */
public class HostEntity implements Cloneable, Serializable {
    private SimpleLongProperty id = new SimpleLongProperty();
    private SimpleBooleanProperty checked = new SimpleBooleanProperty();
    private SimpleStringProperty label = new SimpleStringProperty();
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty ipAddress = new SimpleStringProperty();
    private SimpleStringProperty username = new SimpleStringProperty();
    private SimpleStringProperty password = new SimpleStringProperty();
    private SimpleObjectProperty op = new SimpleObjectProperty();


    public boolean isChecked() {
        return checked.get();
    }

    public long getId() {
        return id.get();
    }

    public SimpleLongProperty idProperty() {
        return id;
    }

    public void setId(long id) {
        this.id.set(id);
    }

    public SimpleBooleanProperty checkedProperty() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked.set(checked);
    }

    public String getLabel() {
        return label.get();
    }

    public SimpleStringProperty labelProperty() {
        return label;
    }

    public void setLabel(String label) {
        this.label.set(label);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getIpAddress() {
        return ipAddress.get();
    }

    public SimpleStringProperty ipAddressProperty() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress.set(ipAddress);
    }

    public String getUsername() {
        return username.get();
    }

    public SimpleStringProperty usernameProperty() {
        return username;
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public String getPassword() {
        return password.get();
    }

    public SimpleStringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public Object getOp() {
        return op.get();
    }

    public SimpleObjectProperty opProperty() {
        return op;
    }

    public void setOp(Object op) {
        this.op.set(op);
    }

    public HostEntityClone toCloneEntity() {
        final HostEntityClone c = new HostEntityClone();
        c.setChecked(this.isChecked());
        c.setName(this.getName());
        c.setLabel(this.getLabel());
        c.setIpAddress(this.getIpAddress());
        c.setUsername(this.getUsername());
        c.setPassword(this.getPassword());
        c.setOp(c);

        return c;
    }


    public static HostEntity from(HostEntityClone c) {
        HostEntity e = new HostEntity();
        e.setName(c.getName());
        e.setChecked(c.isChecked());
        e.setIpAddress(c.getIpAddress());
        e.setUsername(c.getUsername());
        e.setPassword(c.getPassword());
        e.setLabel(c.getLabel());
        e.setOp(e);
        return e;
    }
}
