package com.shixihub.view;

import java.io.Serializable;

/**
 * @author LiShixi 2021/7/2 11:04 上午
 */
public class HostEntityClone implements Serializable {
    private boolean checked = false;
    private String label = "";
    private String name = "";
    private String ipAddress = "";
    private String username = "";
    private String password = "";
    private Object op = new Object();

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Object getOp() {
        return op;
    }

    public void setOp(Object op) {
        this.op = op;
    }
}
