/**
 * @author LiShixi 2021/7/2 11:32 下午
 */
module jump.trans.main {
requires javafx.graphics;
requires javafx.base;
requires javafx.fxml;
    requires javafx.controls;
    requires java.desktop;
    requires jsch;
    opens com.shixihub to javafx.fxml;
    exports com.shixihub;
    exports com.shixihub.view;
    exports com.shixihub.ssh;
    exports com.shixihub.data;
}

