package com.shixihub.util;

import com.shixihub.data.AppDataEntity;
import com.shixihub.data.AppDataManager;
import org.junit.jupiter.api.Assertions;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * @author LiShixi 2021/7/2 2:57 下午
 */
class AppDataManagerTest {

    AppDataManager appDataManager;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        appDataManager = new AppDataManager();
    }

    @org.junit.jupiter.api.AfterEach
    void setDown() {
        deleteDataFile();
    }

    @org.junit.jupiter.api.Test
    void checkDataFileExists() {
        deleteDataFile();
        Assertions.assertFalse(appDataManager.store(new AppDataEntity()).isEmpty());
        Assertions.assertTrue(appDataManager.checkDataFile().isPresent());
    }

    @org.junit.jupiter.api.Test
    void checkFile() {
        deleteDataFile();
        Assertions.assertFalse(appDataManager.checkDataFile().isPresent());
        appDataManager.store(new AppDataEntity());
        Assertions.assertTrue(appDataManager.checkDataFile().isPresent());

    }

    @org.junit.jupiter.api.Test
    void restore() {
        this.store();
        Assertions.assertNotNull(appDataManager.restore());
    }

    @org.junit.jupiter.api.Test
    void store() {
        deleteDataFile();
        Assertions.assertFalse(appDataManager.store(new AppDataEntity()).isEmpty());
    }

    private void deleteDataFile() {

        try {
            final URL resource = AppDataManager.class.getClass().getResource("/" + AppDataManager.APP_DAT_FILE);
            if (Objects.nonNull(resource)) {
                final String file = resource.getFile();
                Files.deleteIfExists(Paths.get(file));
            }
        } catch (IOException e) {
        }
    }
}
